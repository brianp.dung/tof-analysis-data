#define TestTree_cxx
#include "CFDHitFinderAlg.h"
#include "TestTree.h"
#include <TCanvas.h>
#include <TH2.h>
#include <TStyle.h>

void TestTree::Loop() {
  double samplingRateInGSPS = 5;
  std::map<beamlinereco::CFDParams, double> paramSet;
  paramSet[beamlinereco::kADCNBits]                                   = 12;
  paramSet[beamlinereco::kADCDynamicRange]                            = 1;
  paramSet[beamlinereco::kADCOffset]                                  = 0;
  paramSet[beamlinereco::kTimeSamplingInterval]                       = 1 / (samplingRateInGSPS);
  paramSet[beamlinereco::kNSamplingPoints]                            = 1024;
  paramSet[beamlinereco::kIsWaveformNegativePolarity]                 = 1;
  paramSet[beamlinereco::kCFDThreshold]                               = 0.4;
  paramSet[beamlinereco::kRawHitFinderThresholdInNoiseSigma]          = 25;
  paramSet[beamlinereco::kShortRawHitIgnoringDurationInTicks]         = 10;
  paramSet[beamlinereco::kConsecutiveHitSeperationDurationInTicks]    = 5;
  paramSet[beamlinereco::kGSFilter]                                   = 0;
  paramSet[beamlinereco::kGSFilterWindow]                             = 17;
  paramSet[beamlinereco::kGSFilterDegree]                             = 3;
  paramSet[beamlinereco::kIntergratedWindowFixed]                     = 0;
  paramSet[beamlinereco::kIntergratedWindowLowerLimitIndex]           = 0;
  paramSet[beamlinereco::kIntergratedWindowUpperLimitIndex]           = 1024;

  beamlinereco::CFDHitFinder<double>* cfdHF = new beamlinereco::CFDHitFinder<double>();
  cfdHF->SetParams(paramSet);

  if (fChain == 0)
    return;

  Long64_t nentries = fChain->GetEntriesFast();

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry = 30; jentry < 90; jentry++) { // nentries
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0)
      break;
    nb = fChain->GetEntry(jentry);
    nbytes += nb;

    std::vector<double> ChannelHC[8];
    std::vector<uint16_t> ChannelWaveform[8];
    for (unsigned int ch = 1; ch < 9; ch++) {
      double* chPointer = new double[1024];
      switch (ch) {
        case (1): {chPointer = &Channel_1[0]; break;}
        case (2): {chPointer = &Channel_2[0]; break;}
        case (3): {chPointer = &Channel_3[0]; break;}
        case (4): {chPointer = &Channel_4[0]; break;}
        case (5): {chPointer = &Channel_5[0]; break;}
        case (6): {chPointer = &Channel_6[0]; break;}
        case (7): {chPointer = &Channel_7[0]; break;}
        case (8): {chPointer = &Channel_8[0]; break;}
      }

      std::vector<uint16_t> waveform; waveform.clear();
      for (unsigned int k = 0; k < 1024; k++) {
        std::cout << "(uint16_t)*(chPointer + k): " << (uint16_t)*(Channel_1 + k) << std::endl;
        waveform.push_back((uint16_t)*(chPointer + k));
        (ChannelWaveform[ch - 1]).push_back((uint16_t)*(chPointer + k));
      }

      cfdHF->SetWaveform(waveform, ch, jentry);
      cfdHF->Go();
      double baseline = cfdHF->GetPedestal();
      std::map<double, beamlinereco::hit_t<double> > ToFHitCollection = cfdHF->GetHitCollection();

      std::vector<double> hc; hc.clear();
      for (auto hitIter = ToFHitCollection.begin(); hitIter != ToFHitCollection.end(); hitIter++) {
        hc.push_back((*hitIter).first);
      }
      ChannelHC[ch-1] = hc;
    }

    double ticks[1024];
    for (unsigned int i = 0; i < 1024; i++) {
      ticks[i] = (double)i;
    }

    TCanvas* canvas = new TCanvas(Form("canvas_%i", (unsigned int)jentry), "Canvas", 2400, 2400);
    canvas->Divide(2, 4);
    std::vector<TLine*> lines[8];
    for (unsigned int ch = 0; ch < 8; ch++) {
      canvas->cd(ch + 1);

      double adcs[1024];
      for (unsigned int idx = 0; idx < 1024; idx++) {
        adcs[idx] = (double)((ChannelWaveform[ch]).at(idx));
      }

      TGraph* graph = new TGraph(1024, ticks, adcs);
      graph->Draw("AL");

      for (unsigned iHit = 0; iHit < ChannelHC[ch].size(); iHit++) {
        double x     = ChannelHC[ch].at(iHit);
        gPad->Modified();
        gPad->Update();
        double y_min = gPad->GetUymin();
        double y_max = gPad->GetUymax();
        lines[ch].push_back(new TLine(x, y_min, x, y_max));
        lines[ch].at(iHit)->Draw();
        gPad->Modified();
        gPad->Update();
      }
    }
    canvas->SaveAs(Form("Plots/Evt_%i.pdf", (unsigned int)jentry));

  }
}
