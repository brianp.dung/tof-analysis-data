//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr  8 20:53:01 2019 by ROOT version 6.16/00
// from TTree tof_tree/TOF Tree
// found on file: ./TOFTimingResolution.root
//////////////////////////////////////////////////////////

#ifndef TestTree_h
#define TestTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class TestTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        Channel_1[1024];
   Double_t        Channel_2[1024];
   Double_t        Channel_3[1024];
   Double_t        Channel_4[1024];
   Double_t        Channel_5[1024];
   Double_t        Channel_6[1024];
   Double_t        Channel_7[1024];
   Double_t        Channel_8[1024];

   // List of branches
   TBranch        *b_Channel_1;   //!
   TBranch        *b_Channel_2;   //!
   TBranch        *b_Channel_3;   //!
   TBranch        *b_Channel_4;   //!
   TBranch        *b_Channel_5;   //!
   TBranch        *b_Channel_6;   //!
   TBranch        *b_Channel_7;   //!
   TBranch        *b_Channel_8;   //!

   TestTree(TTree *tree=0);
   virtual ~TestTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TestTree_cxx
TestTree::TestTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("./TOFTimingResolution.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("./TOFTimingResolution.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("./TOFTimingResolution.root:/toftimingresolution");
      dir->GetObject("tof_tree",tree);

   }
   Init(tree);
}

TestTree::~TestTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TestTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TestTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TestTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Channel_1", Channel_1, &b_Channel_1);
   fChain->SetBranchAddress("Channel_2", Channel_2, &b_Channel_2);
   fChain->SetBranchAddress("Channel_3", Channel_3, &b_Channel_3);
   fChain->SetBranchAddress("Channel_4", Channel_4, &b_Channel_4);
   fChain->SetBranchAddress("Channel_5", Channel_5, &b_Channel_5);
   fChain->SetBranchAddress("Channel_6", Channel_6, &b_Channel_6);
   fChain->SetBranchAddress("Channel_7", Channel_7, &b_Channel_7);
   fChain->SetBranchAddress("Channel_8", Channel_8, &b_Channel_8);
   Notify();
}

Bool_t TestTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TestTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TestTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TestTree_cxx
